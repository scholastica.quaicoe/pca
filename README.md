```
  title: "PCA_preparation"
author: "Scholastica Quaicoe & Julia Lohr"
date: "3rd June 2022"
output: html_document
```
  
```
knitr::opts_chunk$set(echo = TRUE)
knitr::opts_knit$set(root.dir = "~/")

if (!requireNamespace("BiocManager", quietly = TRUE))
  BiocManager::install("BiocManager")
library("BiocManager")

if (!requireNamespace("varhandle", quietly = TRUE)) # required for unfactor
  BiocManager::install("varhandle")
library("varhandle")

if (!requireNamespace("DESeq2", quietly = TRUE)) # Deseq2
  BiocManager::install("DESeq2")
library("DESeq2")

if (!requireNamespace("dplyr", quietly = TRUE))
  BiocManager::install("dplyr")
library("dplyr")

if (!requireNamespace("stringr", quietly = TRUE)) # for regex
  BiocManager::install("stringr")
library("stringr")
```

# Defining paths

```

# Please specify the full paths as strings (in quotation marks)
out_dir = "/Users/schol/OneDrive/Desktop/Masters thesis/"  # target folder for produced dds.rds objects

d_counts_file = "/Users/schol/Downloads/de_gene_count_matrix3.csv" # source file for counts with new labels
gene_counts = read.csv(d_counts_file, stringsAsFactors = FALSE, row.names = 1)

# check required deseq output exists, else create
if (!dir.exists(out_dir)){
  dir.create(out_dir)
}
```

# Rename samples

```

sample_vector <- colnames(gene_counts)
sample_vector <- strsplit(sample_vector, " ")
prefix = c("D_401", "D_411")

rename_samples= function(samples_vector){
  
  # Rename samples from X401 to D401, or X411 to D411
  
  # Keyword Arguments:
  # samples_vector -- (Vector) Sample names to be renamed
  # prefix -- The prefix to be placed in front of the column name
  
  # Returns:
  # new_sample_names -- (Vector) Modified sample names in same order as input
  
  new_sample_names = vector()
  for (sample in samples_vector){
    if (sample == "gene_id"){
      new_name = sample
    }
    else if (!grepl("X401", sample)) {
      name = str_split(sample, '1_')[[1]][2]
      new_name = paste0("D_401", "_", str_split(sample, '1_')[[1]][2])
    }
    
    else if (!grepl("X411", sample)) {
      name = str_split(sample, '1_')[[1]][2]
      new_name = paste0("D_411", "_", str_split(sample, '1_')[[1]][2])
    }
    
    
    new_sample_names = c(new_sample_names, new_name)
  }
  return(new_sample_names)
}
```

## Create .csv file 
```
#Set Column names
colnames(gene_counts) <- sample_vector

gen_meta_data = function(samples_vector){
  
  # Creates a metadata dataframe from the information in the sample names
  
  # Keyword Arguments:
  #   samples_vector -- (Vector) Sample names with format DE_401_10h_dex_1
  #        {Experiment}_{construct}_{timenum}{timeletter}_{treatment}_{biorepnum}
  
  # Returns:
  # meta_sorted -- (Dataframe) Mostly factors. Each row is a sample and ordered from mock to treatment and start to end.
  
  meta_out = matrix(nrow = 0, ncol = 8)
  for (sample in samples_vector) {
    if (sample != "gene_id"){
      
      reg_group = str_match(sample, "([A-Za-z]*)_([0-9]{3})_([0-9]{2})([h|d])_([\\w]*)_([0-9])?")
      inducer = reg_group[1,2]
      construct = reg_group[1,3]
      time_num = reg_group[1,4]
      time_letter = reg_group[1,5]
      treatment = reg_group[1,6]
      biorep_num = reg_group[1,7]
      
      if (treatment == "mock"){
        treatment = "m"
      }
      if (grepl("dex", treatment)){
        treatment = "ind"
      }
      
      experiment = paste(inducer, construct, sep = "_")
      time_point = paste(time_num, time_letter, sep = "")
      bio_rep = paste(construct, treatment, time_point, biorep_num, sep = "_") 
      sample_point = paste(treatment, time_point, sep = "_")
      exp_timept = paste(experiment, sample_point, sep = "_")
      
      group = c(sample, sample_point, bio_rep, experiment, exp_timept, treatment, time_num, time_letter)
      meta_out = rbind(meta_out, group)
    }
  }
  
  meta_out = varhandle::unfactor(as.data.frame(meta_out))
  colnames(meta_out) = c("name", "timepoint", "biorep", "experiment", "exp_timept", "treatment", "time_num", "time_letter" )
  
  # sorting chronologically
  meta_sorted = meta_out[order(-xtfrm(meta_out$experiment),
                               -xtfrm(meta_out$treatment), 
                               -xtfrm(meta_out$time_letter), 
                               meta_out$time_num, 
                               meta_out$name), ]
  # xtfrm makes text sortable (internally converts to numeric) 
  # '-' means in ascending order, default is descending and '-' undoes this
  
  # need factors for PCA and to maintain order of sorted meta, i need to specify levels!
  meta_sorted$timepoint = factor(meta_sorted$timepoint, levels = unique(meta_sorted$timepoint))
  meta_sorted$biorep = factor(meta_sorted$biorep, levels = unique(meta_sorted$biorep))
  meta_sorted$experiment = factor(meta_sorted$experiment, levels = unique(meta_sorted$experiment))
  meta_sorted$treatment = factor(meta_sorted$treatment, levels = unique(meta_sorted$treatment))
  meta_sorted$exp_timept = factor(meta_sorted$exp_timept, levels = unique(meta_sorted$exp_timept))
  
  return(meta_sorted)
}
```




```
runDESeq <- function(counts_df, formula="~ exp_timept"){
  
  # Generates the DESeq data set form
  
  # Keyword Arguments:
  #   counts_df - (Dataframe) All the gene counts
  
  # Returns:
  #   dds - (S4) DESeq Data Set generated
  
  if(sum(counts_df < 0) == 0) { # test for no negative entries
    counts_df = counts_df[-which(rowSums(counts_df) == 0),] # removes all rows without counts
  }
  
  # generate meta data from column names:
  meta_sorted = gen_meta_data(colnames(counts_df))
  row.names(meta_sorted) = meta_sorted$name
  meta_sorted$name = NULL
  
  # rownames of counts_df need to be in the same order as colnames of meta_sorted:
  counts_df = counts_df[, row.names(meta_sorted)]
  
  apply(counts_df, 2, sum)
  mx <- apply(counts_df, 1, max)
  count_matrix <- counts_df[mx > 10, ]
  dds <- DESeqDataSetFromMatrix(count_matrix, meta_sorted, as.formula(formula))
  # using treatment (ind or m) means that comparisons are made between these to classes, but not within DE or within M
  # dds <- dds[, colnames(dds[, order(dds$construct, -rank(dds$type), dds$time)])]
  dds <- estimateSizeFactors(dds)
  sizeFactors(dds)
  dds <- estimateDispersions(dds)
  dds <- nbinomWaldTest(dds)
  return(dds)
}



# Loading and preprocessing data
# In this section, the gene_expresseion.csv files (output of prepDE) are loaded for the D and DE experiments.
# The metadata required by deseq2 is derived from the columnnames.

if (!file.exists(paste0(out_dir, "deseq_dds.rds"))){
  d_dds = runDESeq(gene_counts)
  saveRDS(d_dds, file = paste0(out_dir, "deseq_dds.rds"))
}
```
